var http = require('http');
var name = 'Muhammad Salman Abid';
const PORT = 2020;
http.createServer(function (req, res) {
  res.write('Hello there! my name is ' + name + '\n');
  res.write(`Hello there! my name is ${name}`);
  res.end();
}).listen(8080);

// "helo there, my name is" +" "+ name ---> concatenation
// `helo there, my name is ${name}` ---> interpolation